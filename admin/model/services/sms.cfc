<cfcomponent>
	<cffunction name="sendSMS" access="public" returntype="boolean">
		<cfargument name="authkey" type="string" required="true" default="#application.msgAuthkey#">
		<cfargument name="mobileNumber" required="true">
		<cfargument name="msg" type="string" required="true">

		<cfset msgURL = "http://api.msg91.com/api/sendhttp.php?route=4&flash=0&unicode=0&campaign=viaSOCKET&authkey=#arguments.authkey#&mobiles=#arguments.mobileNumber#&message=#arguments.msg#&sender=VCLEAN">
		<cfhttp url="#msgURL#" method="get" name="msg91" result="res">
		<cfif res.status_code eq 200>
			<cfreturn true>
		<cfelse>
			<cfreturn false>	
		</cfif>
	</cffunction>
</cfcomponent>