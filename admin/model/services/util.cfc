<cfcomponent output="false">
	
	<cfscript>

		function init(dsn){
			variables.dsn = arguments.dsn;
			return this;
		}

		function QueryToStructureOfStructures(theQuery){
		    var returnStruct = structnew();
		    var cols = ListtoArray(theQuery.columnlist);
		    var row = 1;
		    var col = 1;
		    for(row = 1; row LTE theQuery.recordcount; row = row + 1){
		        var thisRow = structnew();
		        for(col = 1; col LTE arraylen(cols); col = col + 1){
		            thisRow[cols[col]] = theQuery[cols[col]][row];
		        }
		        returnStruct[theQuery["id"][row]] = duplicate(thisRow);
		    }
		    return(returnStruct);
		}

		function QueryToArrayOfStructures(theQuery){
			var theArray = arraynew(1);
		    var cols = ListtoArray(theQuery.columnlist);
		    var row = 1;
		    var thisRow = "";
		    var col = 1;
		    for(row = 1; row LTE theQuery.recordcount; row = row + 1){
		        thisRow = structnew();
		        for(col = 1; col LTE arraylen(cols); col = col + 1){
		            thisRow[cols[col]] = theQuery[cols[col]][row];
		        }
		        arrayAppend(theArray,duplicate(thisRow));
		    }
		    return(theArray);
		}
	</cfscript>

	<cffunction name="genPassword" access="public">
		<!--- Define the list of numbers. --->
		<cfset strList = "a,b,c,d,e,f,g,h,i,j,k,l,o,p,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5" />

		<!---
		    Create a struct to hold the list of selected numbers. Because
		    structs are indexed by key, it will allow us to not select
		    duplicate values.
		--->
		<cfset objSelections = {} />


		<!---
		    Now, all we have to do is pick random numbers until our
		    struct count is the desired size (4 in this demo).
		--->
		<cfloop condition="(StructCount( objSelections ) LTE 8)">

		    <!--- Select a random list index. --->
		    <cfset intIndex = RandRange( 1, ListLen( strList ) ) />

		    <!---
		        Add the random item to our collection. If we have
		        already picked this number, then it will simply
		        overwrite the previous and the StructCount() will
		        not be changed.
		    --->
		    <cfset objSelections[ ListGetAt( strList, intIndex ) ] = true />

		</cfloop>
		<cfreturn ListChangeDelims(StructKeyList( objSelections ),'')>
	</cffunction>
	
</cfcomponent>
