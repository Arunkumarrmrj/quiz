<cfcomponent output="false" extends="BaseDAO">

	<cffunction name="insertPriceInfo" access="public">
		<cfargument name="catID" type="numeric" required="true">
		<cfargument name="priceName" type="string" required="true">
		<cfargument name="price" type="numeric" required="true" >

		<cfset local.qry ="" />

		<cfquery name="local.qry" datasource="#variables.dsn#">
			INSERT INTO price (catID,priceName,price,createdBy,createdAt)
			VALUES (<cfqueryparam value="#arguments.catID#" cfsqltype="cf_sql_integer">,
					<cfqueryparam value="#arguments.priceName#" cfsqltype="cf_sql_varchar">,
					<cfqueryparam value="#arguments.price#" cfsqltype="cf_sql_float">,
					<cfqueryparam value="#session.userID#" cfsqltype="cf_sql_integer">,
					<cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">
				)
		</cfquery>
	</cffunction>

	<cffunction name="updatePriceInfo" access="public">
		<cfargument name="priceID" type="numeric" required="true" />
		<cfargument name="catID" type="numeric" required="true" />
		<cfargument name="priceName" type="string" required="true">
		<cfargument name="price" type="numeric" required="true" >

		<cfset local.qry ="" />

		<cfquery name="local.qry" datasource="#variables.dsn#">
			UPDATE price
			SET catID = <cfqueryparam value='#arguments.catID#' cfsqltype='cf_sql_integer'>,
				priceName = <cfqueryparam value='#arguments.priceName#' cfsqltype='cf_sql_varchar'>,
				price = <cfqueryparam value='#arguments.price#' cfsqltype='cf_sql_float'>,
				updatedBy = <cfqueryparam value='#session.userID#' cfsqltype='cf_sql_integer'>,
				updatedAt = <cfqueryparam value='#now()#' cfsqltype="cf_sql_timestamp">
			WHERE priceID = <cfqueryparam value='#arguments.priceID#' cfsqltype='cf_sql_integer'>
		</cfquery>
	</cffunction>

	<cffunction name="priceList" returntype="query" access="public">
		<cfargument name="id" type="numeric" required="false">

		<cfset var local.qry = "">
		<cfquery name="local.qry" datasource="#variables.dsn#">
			SELECT price.priceID, price.catID, price.priceName, price.price, category.catName
			FROM price
			INNER JOIN category ON price.catID = category.catID
			<cfif structkeyexists(arguments, 'id')>
				AND price.priceID = <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_integer">
			</cfif>
		</cfquery>
		<cfreturn local.qry>
	</cffunction> 
	
</cfcomponent>