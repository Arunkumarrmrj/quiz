<cfcomponent output="false">

	<cffunction name="init" access="public">
		<cfargument name="dsn" hint="data source name" default="">
		<cfset variables.dsn = arguments.dsn>
		<cfreturn this />
	</cffunction>
	
</cfcomponent>
