<cfcomponent extends="BaseDAO" output="false">


	<cffunction name="setModulePermissions" returntype="Any" access="public">
		<cfargument name="roleID" required="true">
		<cfargument name="userID" required="true">

		<cfquery name="qPermissions" datasource="#application.dsn#">
			SELECT p.*, e.empID as userID
			FROM permissions p
			INNER JOIN employee e ON e.roleID = p.roleID
			WHERE p.roleID = <cfqueryparam value="#arguments.roleID#" cfsqltype="cf_sql_integer">
			AND e.empID = <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfset module = {} >
		<cfset permission = {} >
		<cfset actions = ["add","edit","view","delete","nav"] >
		<cfloop query="qPermissions">
			<cfset temp = {} >
			<cfset moduleText = qPermissions.modulename > 
			<cfloop array="#actions#" index="action">
				<cfset val = structKeyExists(qPermissions, action) ? qPermissions[action] : 0 >
				<cfset structInsert( temp, action, val ) >
			</cfloop>
			<cfset structInsert( permission, moduleText, temp ) >
			<cfset qExtra = getExtraPermissionByUserID( userID = arguments.userID, modulename = qPermissions.modulename ) >
			<cfloop query="qExtra">
				<cfset permission[moduleText][lCase(qExtra.action_name)] = qExtra.value >
			</cfloop>
		</cfloop>
		<cfset structInsert( module, 'module', permission ) >
		<cfset structInsert(session, 'permission',module, true)>
	</cffunction>


	<cffunction name="getPermissionsByRole" returntype="Any" access="public">
		<cfargument name="roleID" required="true">

		<cfquery name="qry" datasource="#variables.dsn#">
			SELECT * FROM permissions
			WHERE roleID = <cfqueryparam value="#arguments.roleID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfset permission = {} >
		<cfset actions = ["add","edit","view","delete","nav"] >
		<cfloop query="qry">
			<cfset temp = {} >
			<cfloop array="#actions#" index="action">
				<cfset structInsert( temp, action, qry[action] ) >
			</cfloop>
			<cfset structInsert( permission, modulename, temp ) >
		</cfloop>
		
		<cfreturn permission />
	</cffunction>

	<cffunction name="deleteRolePermisson" returntype="Any" access="public">
		<cfargument name="roleID" required="true">

		<cfquery name="qry" datasource="#variables.dsn#">
			DELETE FROM permissions
			WHERE roleID = <cfqueryparam value="#arguments.roleID#" cfsqltype="cf_sql_integer">
		</cfquery>
		
	</cffunction>

	<cffunction name="createRolePermisson" returntype="Any" access="public">
		<cfargument name="roleID" required="true" type="numeric" >
		<cfargument name="modulename" required="true" type="any" >
		<cfargument name="add" required="false" default="0">
		<cfargument name="edit" required="false" default="0">
		<cfargument name="view" required="false" default="0">
		<cfargument name="delete" required="false" default="0">
		<cfargument name="nav" required="false" default="0">

		<cfquery name="qry" datasource="#variables.dsn#">
			INSERT INTO permissions ( `roleID`, `modulename`, `add`, `edit`, `view`, `delete`, `nav` )
			VALUES (
				<cfqueryparam value="#arguments.roleID#" cfsqltype="cf_sql_integer">,
				<cfqueryparam value="#arguments.modulename#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#arguments.add#" cfsqltype="cf_sql_integer">,
				<cfqueryparam value="#arguments.edit#" cfsqltype="cf_sql_integer">,
				<cfqueryparam value="#arguments.view#" cfsqltype="cf_sql_integer">,
				<cfqueryparam value="#arguments.delete#" cfsqltype="cf_sql_integer">,
				<cfqueryparam value="#arguments.nav#" cfsqltype="cf_sql_integer">
			)
			
		</cfquery>

	</cffunction>

	<cffunction name="getPermissionsByUserID" returntype="Any" access="public">
		<cfargument name="userID" required="true" type="numeric" >
		
		<cfquery name="qry" datasource="#application.dsn#">
			SELECT p.* FROM employee e
			INNER JOIN permissions p ON p.roleID = e.roleID
			WHERE e.empID = <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfreturn qry />
	</cffunction>

	<cffunction name="getExtraPermissionByUserID" returntype="Any" access="public">
		<cfargument name="userID" required="true" type="numeric" >
		<cfargument name="modulename" required="true" type="any" >
		
		<cfquery name="qry" datasource="#application.dsn#">
			SELECT * FROM user_extra_permission
			WHERE userID = <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">
			AND modulename = <cfqueryparam value="#arguments.modulename#" cfsqltype="cf_sql_varchar">
		</cfquery>

		<cfreturn qry />
	</cffunction>

	<cffunction name="createExtraPermission" returntype="Any" access="public">
		<cfargument name="userID" required="true" type="numeric" >
		<cfargument name="modulename" required="true" type="any" >
		<cfargument name="action_name" required="true">
		<cfargument name="value" required="true" >

		<cfquery name="qry" datasource="#application.dsn#">
			INSERT INTO user_extra_permission ( userID, modulename, action_name, value )
			VALUES (
				<cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">,
				<cfqueryparam value="#arguments.modulename#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#arguments.action_name#" cfsqltype="cf_sql_varchar">,
				<cfqueryparam value="#arguments.value#" cfsqltype="cf_sql_integer">
			)
			
		</cfquery>

	</cffunction>

	<cffunction name="deleteExtraPermission" returntype="Any" access="public">
		<cfargument name="userID" required="true" type="numeric" >
		
		<cfquery name="qry" datasource="#application.dsn#">
			DELETE FROM user_extra_permission
			WHERE userID = <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">
		</cfquery>

	</cffunction>

</cfcomponent>