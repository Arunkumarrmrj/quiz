<cfcomponent output="false" extends="BaseDAO">
	
    <cffunction name="priceList" access="public">
        <cfargument name="catID" type="string" required="true">
        
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            SELECT priceID, catID, priceName, price FROM price
            WHERE 1 = 1
            <cfif structkeyexists(arguments, 'catID') AND val(arguments.catID)>
                AND catID = <cfqueryparam value="#arguments.catID#" cfsqltype="cf_sql_integer">
            </cfif>
        </cfquery>
        <cfreturn local.qry>
    </cffunction>

    <cffunction name="orderCreate" access="public">
        <cfargument name="userID" type="numeric" required="true">
        <cfargument name="orderDate" type="date" default="#now()#">
        <cfargument name="orderStatus" type="numeric" default="1">
        <cfargument name="createdBy" type="numeric">
        <cfargument name="deliveryID" type="numeric" default="2">

        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#" result="res">
            INSERT INTO orderdata(userID, orderDate, orderStatus, createdBy, createdAt, deliveryID)
            VALUES(
                <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">,
                <cfqueryparam value="#arguments.orderStatus#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#arguments.createdBy#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">,
                <cfqueryparam value="#arguments.deliveryID#" cfsqltype="cf_sql_integer">
            )
        </cfquery>
        <cfreturn res.generatedkey>
    </cffunction>

    <cffunction name="createOrderdetail" access="public">
        <cfargument name="orderID" type="numeric" required="true">
        <cfargument name="productID" type="numeric" required="true">
        <cfargument name="productCount" type="numeric" required="true">

        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            INSERT INTO orderdetail(orderID, productID, productCount)
            VALUES(<cfqueryparam value="#arguments.orderID#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#arguments.productID#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#arguments.productCount#" cfsqltype="cf_sql_integer">
            )
        </cfquery>
    </cffunction>

    <cffunction name="orderlist" access="public">
        <cfargument name="orderID" type="numeric">
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            SELECT od.orderid, od.orderdate, od.userid,os.orderstatus,os.orderStatusID, dt.deliveryType, u.name, u.email, u.vcid,SUM(p.price*orderdetail.productCount) AS totalprice,SUM(orderdetail.productCount) AS productcount, u.mobile FROM orderdata od
            INNER JOIN orderdetail ON orderdetail.orderid = od.orderid
            INNER JOIN users u ON u.userID = od.userid
            INNER JOIN orderstatus os ON os.orderstatusid = od.orderstatus
            INNER JOIN deliverytype dt ON dt.deliveryID = od.deliveryID
            INNER JOIN price p ON p.priceid = orderdetail.productID
            <cfif structkeyexists(arguments, 'orderID') AND val(arguments.orderID)>
               WHERE od.orderid = <cfqueryparam value="#arguments.orderID#" cfsqltype="cf_sql_integer"> 
            </cfif>
            GROUP BY od.orderid, od.orderdate,od.userid,os.orderstatus, dt.deliveryType, u.name, u.email, u.mobile, u.vcid
        </cfquery>
        <cfreturn local.qry>
    </cffunction>

    <cffunction name="vieworder" access="public">
        <cfargument name="orderID" type="numeric" required="true">
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            SELECT od.productID, od.productCount, p.pricename, p.price FROM orderdetail od
            INNER JOIN price p ON od.productid = p.priceid
            WHERE od.orderID = <cfqueryparam value="#arguments.orderid#" cfsqltype="cf_sql_integer">
        </cfquery>
        <cfreturn local.qry>
    </cffunction>

    <cffunction name="readOrderStatus" access="public">
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            Select * FROM orderStatus
        </cfquery>
        <cfreturn local.qry>
    </cffunction>

    <cffunction name="updateOrderStatus" access="public">
        <cfargument name="orderID" type="any" required="true">
        <cfargument name="statusID" type="any" required="true">
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            UPDATE orderdata
            SET orderStatus = <cfqueryparam value="#arguments.statusID#" cfsqltype="cf_sql_integer">
            WHERE orderid = <cfqueryparam value="#arguments.orderid#" cfsqltype="cf_sql_integer">
        </cfquery>
        <cfif statusID EQ 3>
            <cfset data = orderlist(orderid = arguments.orderid)>
            <cfset msg = "Your clothes are ready to delivery. Total cost is Rs#data.totalprice#. Thank you">
            <cfset Application.smsService.sendSMS(authkey = Application.msgAuthkey, mobilenumber = data.mobile, msg = msg)>
        </cfif>
        
    </cffunction>
	
	
</cfcomponent>

