<cfcomponent output="false" extends="BaseDAO">

	<cffunction name="login" returntype="query">
		<cfargument name="mobileNumber" type="any" required="true" />
		<cfargument name="password" type="any" required="true" />
		
		<cfset local.qry="" />
		
		<cfquery name="local.qry" datasource="#variables.dsn#">
			SELECT e.*, r.roleName FROM employee e
			INNER JOIN role r ON r.roleID = e.roleID
			WHERE e.empMobile = <cfqueryparam value='#arguments.mobileNumber#' cfsqltype="cf_sql_varchar">
			AND e.emppassword = <cfqueryparam value='#arguments.password#' cfsqltype='cf_sql_varchar'>
		</cfquery>
		
		<cfreturn local.qry />
	</cffunction>

	<cffunction name="roleSave" access="public">
		<cfargument name="roleName" type="string" required="true">

		<cfset local.qry ="" />

		<cfquery name="local.qry" datasource="#variables.dsn#">
			INSERT INTO role (roleName, createdBy, createdAt, updatedAT)
			VALUES (<cfqueryparam value="#arguments.roleName#" cfsqltype="cf_sql_varchar">,
					<cfqueryparam value="#session.userID#" cfsqltype="cf_sql_integer">,
					<cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">,
					<cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">
				)
		</cfquery>
	</cffunction>

	<cffunction name="roleUpdate" access="public">
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="roleName" type="string" required="true">

		<cfset var local.qry = "">
		<cfquery name="local.qry" datasource="#variables.dsn#"> 
			UPDATE role
			SET roleName = <cfqueryparam value="#arguments.roleName#" cfsqltype="cf_sql_varchar">
			WHERE roleID = <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_integer">
		</cfquery>
	</cffunction>

	<cffunction name="roleList" returntype="query" access="public">
		<cfargument name="id" type="numeric" required="false">

		<cfset var local.qry = "">
		<cfquery name="local.qry" datasource="#variables.dsn#">
			SELECT * FROM role
			WHERE 1 = 1
			<cfif structkeyexists(arguments, 'id')>
				AND roleID = <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_integer">
			</cfif>
		</cfquery>
		<cfreturn local.qry>
	</cffunction>

	<cffunction name="BranchSave" access="public">
		<cfargument name="BranchName" type="string" required="true">

		<cfset local.qry ="" />

		<cfquery name="local.qry" datasource="#variables.dsn#">
			INSERT INTO branch (branchName, createdBy, createdAt, updatedAT)
			VALUES (<cfqueryparam value="#arguments.branchName#" cfsqltype="cf_sql_varchar">,
					<cfqueryparam value="#session.userID#" cfsqltype="cf_sql_integer">,
					<cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">,
					<cfqueryparam value="#now()#" cfsqltype="cf_sql_timestamp">
				)
		</cfquery>
	</cffunction>

	<cffunction name="branchUpdate" access="public">
		<cfargument name="id" type="numeric" required="true">
		<cfargument name="branchName" type="string" required="true">

		<cfset var local.qry = "">
		<cfquery name="local.qry" datasource="#variables.dsn#"> 
			UPDATE branch
			SET branchName = <cfqueryparam value="#arguments.branchName#" cfsqltype="cf_sql_varchar">
			WHERE branchID = <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_integer">
		</cfquery>
	</cffunction>

	<cffunction name="branchList" returntype="query" access="public">
		<cfargument name="id" type="numeric" required="false">

		<cfset var local.qry = "">
		<cfquery name="local.qry" datasource="#variables.dsn#">
			SELECT * FROM branch
			WHERE 1 = 1
			<cfif structkeyexists(arguments, 'id')>
				AND branchID = <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_integer">
			</cfif>
		</cfquery>
		<cfreturn local.qry>
	</cffunction>
	
</cfcomponent>