<cfcomponent output="false" extends="BaseDAO">

    <cffunction name="readUser" access="public">
        <cfargument name="mobileNumber" type="any" required="false">
        <cfargument name="password" type="any" required="false">
        <cfargument name="userID" type="any" required="false">

        <cfset local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            SELECT * FROM Users
            WHERE 1 = 1 
            <cfif structkeyexists(arguments, 'mobileNumber') AND structkeyexists(arguments, 'password')>
                AND mobile = <cfqueryparam value="#arguments.mobileNumber#" cfsqltype="cf_sql_varchar">
                AND password = <cfqueryparam value="#arguments.password#" cfsqltype="cf_sql_varchar">
            </cfif>
            <cfif structkeyexists(arguments, 'userID') AND val(arguments.userID)>
               AND userID = <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer"> 
            </cfif>
        </cfquery>
        <cfreturn local.qry>
    </cffunction>
	
    <cffunction name="cityData" access="public">
        <cfargument name="cityID" type="numeric" required="false">
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            SELECT * FROM city
            WHERE 1=1
            <cfif structkeyexists(arguments,'cityID') AND val(arguments.cityID)>
                AND cityID = <cfqueryparam value="#arguments.cityID#" cfsqltype="cf_sql_integer">
            </cfif>
        </cfquery>
        <cfreturn local.qry>
    </cffunction>

    <cffunction name="createUser" access="public">
        <cfargument name="name" type="any" required="true">
        <cfargument name="email" type="any" required="true">
        <cfargument name="mobileNumber" type="any" required="true">
        <cfargument name="password" type="any" required="true">
        <cfargument name="cityID" type="any" required="true">

        <cfset local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#" result="res">
            INSERT INTO Users (name, email, mobile, password, ipaddress, cityID )
            VALUES (<cfqueryparam value="#arguments.name#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.email#" cfsqltype="cf_sql_varchar">,  
                <cfqueryparam value="#arguments.mobileNumber#" cfsqltype="cf_sql_varchar">,  
                <cfqueryparam value="#arguments.password#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#cgi.REMOTE_ADDR#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.cityID#" cfsqltype="cf_sql_integer">
            )
        </cfquery>
        <cfreturn res.generatedkey>
    </cffunction>

    <cffunction name="saveVCID" access="public">
        <cfargument name="vcID" type="string" required="true">
        <cfargument name="userID" type="any" required="true">
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            UPDATE Users
            SET vcID = <cfqueryparam value="#arguments.vcID#" cfsqltype="cf_sql_varchar">
            WHERE userID = <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">
        </cfquery>
    </cffunction>

    <cffunction name="orderSearch" access="public">
        <cfargument name="mobileNumber" type="string" required="true">
        
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            SELECT userID, mobile, Name, vcID, c.cityName FROM Users
            INNER JOIN city c ON Users.CityID = c.CityID 
            WHERE mobile LIKE <cfqueryparam value="%#arguments.mobileNumber#%" cfsqltype="cf_sql_varchar">
            OR vcID LIKE <cfqueryparam value="%#arguments.mobileNumber#%" cfsqltype="cf_sql_varchar">
        </cfquery>
        <cfreturn local.qry>
    </cffunction>
	
	
</cfcomponent>

