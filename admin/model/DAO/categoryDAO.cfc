<cfcomponent output="false" extends="BaseDAO">

	<cffunction name="insertCategoryInfo" access="public">
		<cfargument name="catName" type="string" required="true">

		<cfset local.qry ="" />

		<cfquery name="local.qry" datasource="#variables.dsn#">
			INSERT INTO category (catName)
			VALUES (<cfqueryparam value="#arguments.catName#" cfsqltype="cf_sql_varchar">)
		</cfquery>
	</cffunction>

	<cffunction name="updateCategoryInfo" access="public">
		<cfargument name="catID" type="numeric" required="true" />
		<cfargument name="catName" type="string" required="true">

		<cfset local.qry ="" />

		<cfquery name="local.qry" datasource="#variables.dsn#">
			UPDATE category
			SET catName = <cfqueryparam value='#arguments.catName#' cfsqltype='cf_sql_varchar'>
			WHERE catID = <cfqueryparam value='#arguments.catID#' cfsqltype='cf_sql_integer'>
		</cfquery>
	</cffunction>

	<cffunction name="categoryList" returntype="query" access="public">
		<cfargument name="id" type="numeric" required="false">

		<cfset var local.qry = "">
		<cfquery name="local.qry" datasource="#variables.dsn#">
			SELECT * FROM category
			WHERE 1 = 1
			<cfif structkeyexists(arguments, 'id')>
				AND catID = <cfqueryparam value="#arguments.id#" cfsqltype="cf_sql_integer">
			</cfif>
		</cfquery>
		<cfreturn local.qry>
	</cffunction>
	
</cfcomponent>