<cfcomponent output="false" extends="BaseDAO">
	
    <cffunction name="read" access="public">
        <cfargument name="userID" type="string" required="false">
        
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            SELECT e.*, r.roleName, B.BranchName FROM employee e
            INNER JOIN role r ON r.roleID = e.roleID
            INNER JOIN branch b ON b.branchID = e.empBranchID
            <cfif structKeyExists(arguments, "userID")>
                WHERE e.empID = <cfqueryparam value="#arguments.userID#" cfsqltype="cf_sql_integer">                
            </cfif>
        </cfquery>
        
        <cfreturn local.qry>
    </cffunction>

    <cffunction name="empSave" access="public">
        <cfargument name="empName" type="any" required="true">
        <cfargument name="empEmail" type="any" required="true">
        <cfargument name="empMobile" type="any" required="true">
        <cfargument name="empPassword" type="any" required="true">
        <cfargument name="empDOB" type="any" required="true">
        <cfargument name="empStreet" type="any" required="true">
        <cfargument name="empCity" type="any" required="true">
        <cfargument name="empZip" type="any" required="true">
        <cfargument name="empRole" type="any" required="true">
        <cfargument name="empGender" type="any" required="true">
        <cfargument name="empBloodGroup" type="any" required="true">
        <cfargument name="empEmergencyContactName" type="any" required="true">
        <cfargument name="empEmergencyContactMobile" type="any" required="true">
        <cfargument name="empQualification" type="any" required="true">
        <cfargument name="empBranchID" type="any" required="true">
        <cfargument name="createdBY" type="any" default="#session.userID#">
        <cfargument name="createdAt" type="any" default="#now()#">
        <cfargument name="updatedAt" type="any" default="#now()#">
        <cfset var local.qry = "">
        <cfquery name="local.qry" datasource="#variables.dsn#">
            INSERT INTO employee(empName, empEmail, empMobile, emppassword, empDOB, empStreet, empCity, empPincode, empGender, empEmergencyContactName, empEmergencymobile, empQualification, roleID, empBranchID, createdBY, createdAt, empBloodGroup, updatedAt)
            VALUES(<cfqueryparam value="#arguments.empname#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empEmail#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empMobile#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empPassword#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empDOB#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empStreet#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empCity#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empZip#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empGender#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#arguments.empEmergencyContactName#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empEmergencyContactMobile#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empQualification#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.empRole#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#arguments.empBranchID#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#arguments.createdBY#" cfsqltype="cf_sql_integer">,
                <cfqueryparam value="#arguments.createdAt#" cfsqltype="cf_sql_timestamp">,
                <cfqueryparam value="#arguments.empBloodGroup#" cfsqltype="cf_sql_varchar">,
                <cfqueryparam value="#arguments.createdAt#" cfsqltype="cf_sql_timestamp">
            )
        </cfquery>
    </cffunction>
	
	
</cfcomponent>

