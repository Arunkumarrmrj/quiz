<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Create Question </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="<cfoutput>#buildurl('questions.saveorder')#</cfoutput>" method="post">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">User Search</label>
                            <input class="typeahead form-control" type="text">
                        </div>
                        <input type="hidden" name="userID" id="userID">
                        <div id="userInfo" class="hide">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Customer Name</label>
                                        <input type="text" id="cName" class="form-control" placeholder="Chee Kin">
                                        <span class="help-block"> This is inline help </span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Customer ID</label>
                                        <input type="text" id="cID" class="form-control" placeholder="Chee Kin">
                                        <span class="help-block"> This is inline help </span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Customer Mobile</label>
                                        <input type="text" id="cMobile" class="form-control" placeholder="Chee Kin">
                                        <span class="help-block"> This is inline help </span>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Customer City</label>
                                        <input type="text" id="cCity" class="form-control" placeholder="Chee Kin">
                                        <span class="help-block"> This is inline help </span>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Delivery Type</label>
                                        <!--- <div class="col-md-3"> --->
                                            <select class="form-control" name="deliveryID" required>
                                                <option value="1">Home Delivery</option>
                                                <option value="2">Point of Delivery</option>
                                            </select>
                                        <!--- </div> --->
                                    </div>
                                </div>    
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light bordered">
                                <!--- <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-blue"></i>
                                        <span class="caption-subject font-blue bold uppercase">Overview</span>
                                        <span class="caption-helper">report overview...</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group">
                                            <a class="btn green btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> All Project </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> AirAsia </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Cruise </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> HSBC </a>
                                                </li>
                                                <li class="divider"> </li>
                                                <li>
                                                    <a href="javascript:;"> Pending
                                                        <span class="badge badge-danger"> 4 </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Completed
                                                        <span class="badge badge-success"> 12 </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Overdue
                                                        <span class="badge badge-warning"> 9 </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> --->
                                <div class="portlet-body">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#overview_1" data-toggle="tab"> Men </a>
                                            </li>
                                            <li>
                                                <a href="#overview_2" data-toggle="tab"> Women </a>
                                            </li>
                                            <li>
                                                <a href="#overview_3" data-toggle="tab"> Others </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="overview_1">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th> Name </th>
                                                                <th> Price </th>
                                                                <th> Quantity</th>
                                                                <th> Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <cfoutput query="rc.malePrice">
                                                              <tr>
                                                                <td>
                                                                    <label class="mt-checkbox">
                                                                        <input type="checkbox" name="price" id="inlineCheckbox2" class="priceChkBox" value="#priceID#">
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>#priceName#</td>
                                                                <td>Rs #price#</td>
                                                                <td><input type="number" name="price_#priceID#" class="pq" data-price="#price#" data-id="#priceID#" placeholder="" min="1" disabled=""></td>
                                                                <td><input type="number" name="" value="" placeholder="" id="priceAmount#priceID#" disabled=""></td>
                                                              </tr>  
                                                            </cfoutput>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="overview_2">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th> Product Name </th>
                                                                <th> Price </th>
                                                                <th> Views </th>
                                                                <th> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Metronic - Responsive Admin + Frontend Theme </a>
                                                                </td>
                                                                <td> $20.00 </td>
                                                                <td> 11190 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Regatta Luca 3 in 1 Jacket </a>
                                                                </td>
                                                                <td> $25.50 </td>
                                                                <td> 1245 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Motorola Droid 4 XT894 - 16GB - Black </a>
                                                                </td>
                                                                <td> $878.50 </td>
                                                                <td> 784 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Apple iPhone 4s - 16GB - Black </a>
                                                                </td>
                                                                <td> $625.50 </td>
                                                                <td> 809 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Samsung Galaxy S III SGH-I747 - 16GB </a>
                                                                </td>
                                                                <td> $915.50 </td>
                                                                <td> 6709 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Motorola Droid 4 XT894 - 16GB - Black </a>
                                                                </td>
                                                                <td> $878.50 </td>
                                                                <td> 784 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="overview_3">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th> Customer Name </th>
                                                                <th> Total Orders </th>
                                                                <th> Total Amount </th>
                                                                <th> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> David Wilson </a>
                                                                </td>
                                                                <td> 3 </td>
                                                                <td> $625.50 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Amanda Nilson </a>
                                                                </td>
                                                                <td> 4 </td>
                                                                <td> $12625.50 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Paul Strong </a>
                                                                </td>
                                                                <td> 1 </td>
                                                                <td> $890.85 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Jhon Doe </a>
                                                                </td>
                                                                <td> 2 </td>
                                                                <td> $125.00 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Bill Chang </a>
                                                                </td>
                                                                <td> 45 </td>
                                                                <td> $12,125.70 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Paul Strong </a>
                                                                </td>
                                                                <td> 1 </td>
                                                                <td> $890.85 </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="overview_4">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-hover table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th> Customer Name </th>
                                                                <th> Date </th>
                                                                <th> Amount </th>
                                                                <th> Status </th>
                                                                <th> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> David Wilson </a>
                                                                </td>
                                                                <td> 3 Jan, 2013 </td>
                                                                <td> $625.50 </td>
                                                                <td>
                                                                    <span class="label label-sm label-warning"> Pending </span>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Amanda Nilson </a>
                                                                </td>
                                                                <td> 13 Feb, 2013 </td>
                                                                <td> $12625.50 </td>
                                                                <td>
                                                                    <span class="label label-sm label-warning"> Pending </span>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Paul Strong </a>
                                                                </td>
                                                                <td> 1 Jun, 2013 </td>
                                                                <td> $890.85 </td>
                                                                <td>
                                                                    <span class="label label-sm label-success"> Success </span>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Jhon Doe </a>
                                                                </td>
                                                                <td> 20 Mar, 2013 </td>
                                                                <td> $125.00 </td>
                                                                <td>
                                                                    <span class="label label-sm label-success"> Success </span>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Bill Chang </a>
                                                                </td>
                                                                <td> 29 May, 2013 </td>
                                                                <td> $12,125.70 </td>
                                                                <td>
                                                                    <span class="label label-sm label-info"> In Process </span>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a href="javascript:;"> Paul Strong </a>
                                                                </td>
                                                                <td> 1 Jun, 2013 </td>
                                                                <td> $890.85 </td>
                                                                <td>
                                                                    <span class="label label-sm label-success"> Success </span>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="btn btn-sm btn-default">
                                                                        <i class="fa fa-search"></i> View </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>  
                    </div>
                    <div class="form-actions">
                        <div class="btn-set pull-right">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

<script>
    function formatRepo(repo) {

        //console.log(repo.mobile)
        if (repo.loading) return repo.text;

        var markup = "<div class='select2-result-repository clearfix'>" +
             "<div class='select2-result-repository__title'>" + repo.mobile + "</div>"+
             "</div>";
        return markup;    
    }

    function formatRepoSelection(repo) {
        return repo.mobile;
    }
</script>