<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Question List</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<cfoutput>#buildurl('questions.addEdit')#</cfoutput>" id="sample_editable_1_new" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a> 
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable question-column" id="sample_1">
                    <thead>
                        <tr>
                            <th> Question ID </th>
                            <th> Question Name </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput query="rc.questionData">
                            <tr class="odd gradeX">
                                <td>#questionID#</td>
                                <td>#question#</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a href="javascript:;" data-questionID="#questionID#" class="view-order">
                                                    <i class="icon-docs"></i> View </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" data-questionID="#questionID#" data-status="#orderStatusID#" data-vcid="#vcid#" class="update-status">
                                                    <i class="icon-tag"></i> Update Status </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>    
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
