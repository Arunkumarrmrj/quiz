<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Branch List</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<cfoutput>#buildurl('branch.addedit')#</cfoutput>" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th> Branch </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput query="rc.branchData">
                            <tr class="odd gradeX">
                                <td>#CurrentRow#</td>
                                <td>#branchName#</td>
                                <td>
                                    <a href="#buildurl('#getsection()#.addedit?id=#branchid#')#" class="btn btn-icon-only blue">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-icon-only red">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>   
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>