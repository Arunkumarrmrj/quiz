<div class="row">
	<div class="col-md-12 ">
		<div class="portlet box yellow ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Role </div>
                <div class="tools">
                    <a href="" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="post" action="<cfoutput>#buildurl('branch.branchSave')#</cfoutput>" id="form_sample_1">
                    <input type="hidden" name="id" value="<cfoutput>#rc.branchData.branchID#</cfoutput>">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                        </div>
                        <div class="form-group">
                            <label>Branch Name <span class="required"> * </span></label>
                            <input type="text" class="form-control input-lg" data-required="1" placeholder="Branch Name" name="branchname" value="<cfoutput>#rc.branchData.branchName#</cfoutput>">
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default cancel-btn">Cancel</button>
                        <button type="submit" class="btn green">Submit</button> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>