<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Role List</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<cfoutput>#buildurl('role.addedit')#</cfoutput>" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th> Role </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput query="rc.roleData">
                            <tr class="odd gradeX">
                                <td>#CurrentRow#</td>
                                <td>#roleName#</td>
                                <td>
                                    <a href="#buildurl('#getsection()#.addedit?id=#roleid#')#" class="btn btn-icon-only blue">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="javascript:;" class="btn btn-icon-only red">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>   
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>