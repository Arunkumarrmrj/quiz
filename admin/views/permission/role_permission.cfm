<cfoutput>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Assign/Manage Role Permissions </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="#buildURL('permission.save_role_permissions')#" method="post"  >
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label">Role</label>
                                <select class="form-control" name="role" id="role" onchange="getPermissonsByRole(this.value);" >
                                    <cfoutput query="rc.roles">
                                        <option value="#roleID#">#roleName#</option>
                                    </cfoutput>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="mt-checkbox-inline">
                                    <label class="mt-checkbox">
                                        <input type="checkbox" id="checkAll" value="1"> All Modules
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <cfloop array="#rc.modules#" index="module">
                                <input type="hidden" name="moduleName" value="#module#" />
                                <div class="form-group module-wrap">
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" id="checkModule" class="modules" value="1" > #module# Module
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="mt-checkbox-inline">
                                        <cfloop array="#rc.actions#" index="action">
                                            <label class="mt-checkbox">
                                                <input type="checkbox" name="name.#module#.#action#" id="name_#lCase(module)#_#lCase(action)#" value="1" class="permissions" > #action#
                                                <span></span>
                                            </label>
                                        </cfloop>
                                    </div>

                                    <br>
                                </div>
                            </cfloop>
                        </div>
                        <div class="form-actions">
                            <div class="btn-set pull-right">
                                <button type="submit" class="btn green">Save Permissions</button>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>

<cfsavecontent variable="request.inlineJS">
    #request.inlineJS#
    
    <script type="text/javascript">
        
        $(function(){
            $("##role").trigger('change');
            $("##role").on("change", function(){
                $(".permissions").prop("checked",false);
            });

            $("##checkAll").click(function(){
                if( $(this).is(":checked") ) {
                    $(".modules").prop("checked", true );
                    $(".permissions").prop("checked", true );
                } else {
                    $(".modules").prop("checked", false );
                    $(".permissions").prop("checked", false );
                }
            });

            $(".modules").click(function(){
                if( $(this).is(":checked") ) {
                    $(this).parents(".module-wrap").find(".permissions").prop("checked", true );
                } else {
                    $(this).parents(".module-wrap").find(".permissions").prop("checked", false );
                }   
            });
        });

        function getPermissonsByRole( roleID ) {
            $.post("<cfoutput>#buildURL('permission.ajax_permissonsByRole')#</cfoutput>&r="+Math.random(), {
                roleID : roleID
            }, function(data) {
                $.each(data, function(i, val) {
                    var modulename = i;
                    for( var key in val ) {
                        if( val[key] )
                            var checked = true;
                        else
                            var checked = false;

                        $($("##name_"+modulename.toLowerCase()+"_"+key.toLowerCase())).prop('checked', checked );
                    }
                });
            });
        }
    </script>
</cfsavecontent>
</cfoutput>