<div class="row">
    <div class="col-md-12">
        <div class="portlet box yellow">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Assign Permission </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="#">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Role</label>
                            <select class="form-control">
                                <cfoutput query="rc.roleData">
                                    <option value="#roleID#">#roleName#</option>
                                </cfoutput>
                            </select>
                        </div>
                        <cfloop array="#Application.module#" index="module">
                            <div class="form-group">
                                <label><cfoutput>#module# Module</cfoutput></label>
                                <div class="mt-checkbox-inline">
                                    <label class="mt-checkbox">
                                        <input type="checkbox" id="inlineCheckbox1" value="option1"> Checkbox 1
                                        <span></span>
                                    </label>
                                    <label class="mt-checkbox">
                                        <input type="checkbox" id="inlineCheckbox2" value="option2"> Checkbox 2
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </cfloop>
                    </div>
                    <div class="form-actions">
                        <div class="btn-set pull-right">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>