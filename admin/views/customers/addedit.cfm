<div class="row">
    <div class="col-md-12 ">
        <div class="portlet box yellow ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Customer </div>
                <div class="tools">
                    <a href="" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="post" action="<cfoutput>#buildurl('customers.customersave')#</cfoutput>" id="form_sample_1">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                        </div>
                        <div class="form-group">
                            <label>Name <span class="required"> * </span></label>
                            <input type="text" class="form-control input-lg" data-required="1" placeholder="Customer Name" name="customerName">
                        </div>
                        <div class="form-group">
                            <label>Email <span class="required"> * </span></label>
                            <input type="email" class="form-control input-lg" data-required="1" placeholder="Customer Email" name="customerEmail">
                        </div>
                        <div class="form-group">
                            <label>Mobile <span class="required"> * </span></label>
                            <input type="text" class="form-control input-lg" data-required="1" placeholder="Customer Mobile" name="customerMobile">
                        </div>
                        <div class="form-group">
                            <label>Password <span class="required"> * </span></label>
                            <input type="text" class="form-control input-lg" data-required="1" placeholder="Customer Password" name="customerPassword" value="<cfoutput>#rc.customerPassword#</cfoutput>">
                        </div>
                        <div class="form-group">
                            <label>City <span class="required"> * </span></label>
                            <select class="form-control input-lg" name="customercityID">
                                <cfoutput query="rc.citydata">
                                    <option value="#cityID#">#cityName#</option>
                                </cfoutput>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn green">Submit</button> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>