<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Customers List</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <cfif val(session.permission.module[rc.modulename].add)>
                        <div class="col-md-6">
                            <div class="btn-group">
                                <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        </cfif>
                        <!--- <div class="col-md-6">
                            <div class="btn-group pull-right">
                                <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-print"></i> Print </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                    </li>
                                </ul>
                            </div>
                        </div> --->
                    </div>
                </div>
                <cfif val(session.permission.module[rc.modulename].view) >
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Mobile </th>
                            <th> ID </th>
                            <cfif val(session.permission.module[rc.modulename].edit) OR val(session.permission.module[rc.modulename].delete) >
                            <th> Actions </th>
                            </cfif>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput query="rc.customerData">
                            <tr class="odd gradeX">
                                <td> #Name# </td>
                                <td>
                                    <a href="mailto:#email#"> #email# </a>
                                </td>
                                <td>
                                    #mobile#
                                </td>
                                <td class="center"> #vcID# </td>
                                <cfif val(session.permission.module[rc.modulename].edit) OR val(session.permission.module[rc.modulename].delete) >
                                    <td>
                                        <a href="#buildurl('customers.addedit?id=#userID#')#" class="btn btn-icon-only blue">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:;" class="btn btn-icon-only red">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </cfif>
                            </tr>
                        </cfoutput>
                    </tbody>
                </table>
                </cfif>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>