<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Employee </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form" style="display: block;">
        <!-- BEGIN FORM-->
        <form action="<cfoutput>#buildurl('Employee.save')#</cfoutput>" class="horizontal-form" method="post">
            <div class="form-body">
                <h3 class="form-section">Person Info</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Name</label>
                            <input type="text" id="empName" class="form-control" placeholder="Employee Name" name="empName">
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" >Gender</label>
                            <select class="form-control" name="empGender">
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Date of Birth</label>
                            <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="empDOB"> 
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Qualification</label>
                            <input type="text" id="empQualification" class="form-control" placeholder="Qualification" name="empQualification">
                        </div>
                    </div>
                </div>
                <!--/row-->
                <!--/row-->
                <div class="row">
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" id="empEmail" class="form-control" placeholder="Email" name="empEmail">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Mobile Number</label>
                            <input type="text" id="empMobile" class="form-control" placeholder="Mobile Number" name="empMobile">
                            <span class="help-block"> This is the USERNAME of the Employee </span>
                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Role</label>
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="empRole">
                                <option value="Category 1">Select Role</option>
                                <cfoutput query="rc.roleData">
                                    <option value="#roleID#">#roleName#</option>
                                </cfoutput>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input type="password" id="empPassword" class="form-control" placeholder="Password" name="empPassword">
                            <span class="help-block"> This is the PASSWORD of the Employee </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Branch</label>
                            <select class="form-control" data-placeholder="Choose a Branch" tabindex="1" name="empBranchID">
                                <cfoutput query="rc.branchData">
                                    <option value="#branchID#">#branchName#</option>
                                </cfoutput>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Blood Group</label>
                            <input type="test" id="empBloodGroup" class="form-control" placeholder="Blood Group" name="empBloodGroup" maxlength="6">
                        </div>
                    </div>
                </div>
                <!--/row-->
                <h3 class="form-section">Address</h3>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label>Street</label>
                            <input type="text" class="form-control" name="empAddress"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" class="form-control" name="empCity"> 
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Post Code</label>
                            <input type="text" class="form-control" name="empZip"> 
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <!--/row-->
                <h3 class="form-section">Emergency contact details</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name of contact</label>
                            <input type="text" class="form-control" name="empEmergencyContactName"> </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" class="form-control" name="empEmergencyContactMobile"> </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
            </div>
            <div class="form-actions right">
                <button type="button" class="btn default cancel-btn">Cancel</button>
                <button type="submit" class="btn blue">
                    <i class="fa fa-check"></i> Save</button>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>