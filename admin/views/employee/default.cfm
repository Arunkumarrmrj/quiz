<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Employee Table</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<cfoutput>#buildurl('employee.addedit')#</cfoutput>" id="sample_editable_1_new" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th> Employee Name </th>
                            <th> Email </th>
                            <th> Mobile </th>
                            <th>Branch</th>
                            <th>Role</th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput query="rc.empData">
                            <tr class="odd gradeX">
                                <td>#empname#</td>
                                <td>#empEmail#</td>
                                <td>#empMobile#</td>
                                <td>#branchName#</td>
                                <td>#roleName#</td>
                                <td>
                                    <cfif val(session.permission.module[rc.modulename].edit) >
                                        <a href="#buildURL('employee.addedit?Id=#empID#')#" class="btn btn-icon-only blue">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </cfif>
                                    <cfif val(session.permission.module[rc.modulename].delete) >
                                        <a href="javascript:;" class="btn btn-icon-only red">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </cfif>
                                </td>
                            </tr>    
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>