<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Category List</span>
                </div>
            </div>
            <div class="portlet-body">
                <cfif val(session.permission.module[rc.modulename].add) >
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<cfoutput>#buildurl('price.addedit')#</cfoutput>" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                </cfif>
                <cfif val(session.permission.module[rc.modulename].view) >                    
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Category</th>
                            <th>Price Name</th>
                            <th>Price</th>
                            <cfif val(session.permission.module[rc.modulename].edit) OR val(session.permission.module[rc.modulename].delete) >
                            <th>Actions</th>
                            </cfif>
                        </tr>
                    </thead>
                    <tbody>
                        <cfloop query="rc.priceData">
                            <cfoutput>
                                <tr class="odd gradeX">
                                    <td>#CurrentRow#</td>
                                    <td class="text-center">#catName#</td>
                                    <td>#priceName#</td>
                                    <td class="text-center">#price#</td>
                                    <cfif val(session.permission.module[rc.modulename].edit) OR val(session.permission.module[rc.modulename].delete) >
                                        
                                    <td>
                                        <cfif val(session.permission.module[rc.modulename].edit) >
                                        <a href="<cfoutput>#buildURL('price.addedit?Id=#rc.priceData.priceID#')#</cfoutput>" class="btn btn-icon-only blue">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        </cfif>
                                        <cfif val(session.permission.module[rc.modulename].delete) >
                                        <a href="javascript:;" class="btn btn-icon-only red">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        </cfif>
                                    </td>
                                    </cfif>
                                </tr> 
                            </cfoutput>  
                        </cfloop>
                    </tbody>
                </table>
                </cfif>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div> --->