<cfoutput>
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>
                <cfif val(url.id)>
                    Edit Price
                <cfelse>
                    Add Price
                </cfif> 
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title="">
                </a>
            </div>
        </div>
        <div class="portlet-body form" style="display: block;">
            <!-- BEGIN FORM-->
            <form action="<cfoutput>#buildurl('price.save')#</cfoutput>" method="post" class="horizontal-form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Category</label>
                                <input type="hidden" name="priceID" id="priceID" value="#rc.priceData.priceID#">
                                <select class="form-control" name="catID">
                                    <cfloop query="rc.catList">
                                        <option value="#rc.catList.catID#" <cfif rc.catList.catID eq rc.priceData.catID>selected</cfif>>#rc.catList.catName#</option>                           
                                    </cfloop>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Price Name</label>
                                <input type="text" id="priceName" class="form-control" placeholder="Price Name" name="priceName" value="#rc.priceData.priceName#">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Price</label>
                                <input type="text" id="price" class="form-control" placeholder="Price" name="price" value="#rc.priceData.price#">
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions right">
                    <button type="button" class="btn default">Cancel</button>
                    <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</cfoutput>