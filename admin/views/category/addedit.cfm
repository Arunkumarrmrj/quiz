<cfoutput>
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>
                <cfif val(url.id)>
                    Edit Category
                <cfelse>
                    Add Category
                </cfif> 
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse" data-original-title="" title="">
                </a>
            </div>
        </div>
        <div class="portlet-body form" style="display: block;">
            <!-- BEGIN FORM-->
            <form action="<cfoutput>#buildurl('category.save')#</cfoutput>" method="post" class="horizontal-form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="hidden" name="catID" id="catID" value="#rc.qData.catID#">
                                <label class="control-label">Name</label>
                                <input type="text" id="catName" class="form-control" placeholder="Category Name" name="catName" value="#rc.qData.catName#">
                            </div>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions right">
                    <button type="button" class="btn default">Cancel</button>
                    <button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</cfoutput>