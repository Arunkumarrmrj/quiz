<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> Order List</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="<cfoutput>#buildurl('order.addEdit')#</cfoutput>" id="sample_editable_1_new" class="btn sbold green"> Add New
                                    <i class="fa fa-plus"></i>
                                </a> 
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th> OrderID </th>
                            <th> Customer Name </th>
                            <th> Order Date </th>
                            <th> Order Status </th>
                            <th> Delivery Type </th>
                            <th> Cloths Count </th>
                            <th> Total </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        <cfoutput query="rc.orderData">
                            <tr class="odd gradeX">
                                <td>#orderID#</td>
                                <td>#name#</td>
                                <td>#datetimeformat(orderDate, 'dd mmm yyyy hh:nn tt')#</td>
                                <td><span class="label label-sm label-<cfif orderStatusID EQ 1 OR orderStatusID EQ 4>danger<cfelse>success</cfif>"> #orderStatus# </span></td>
                                <td><span class="label label-sm label-<cfif deliveryType EQ 1>danger<cfelse>info</cfif>"> #deliveryType# </span></td>
                                <td>#Productcount#</td>
                                <td>#totalPrice#</td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu pull-left" role="menu">
                                            <li>
                                                <a href="javascript:;" data-orderid="#orderID#" class="view-order">
                                                    <i class="icon-docs"></i> View </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" data-orderid="#orderID#" data-status="#orderStatusID#" data-vcid="#vcid#" class="update-status">
                                                    <i class="icon-tag"></i> Update Status </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>    
                        </cfoutput>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="760" aria-hidden="false" style="display: block; width: 760px; margin-left: -380px; margin-top: 0px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Order Detail</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead>
                    <tr>
                        <th> Name </th>
                        <th> Price </th>
                        <th> Count </th>
                        <th> Total </th>
                    </tr>
                </thead>
                <tbody id="pData">

                <tbody>    
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <!--- <button type="button" class="btn green">Save changes</button> --->
    </div>
</div>

<div id="status-update" class="modal fade" tabindex="-1" data-width="760" aria-hidden="false" style="display: block; width: 760px; margin-left: -380px; margin-top: 0px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Update Status</h4>
    </div>
    <div class="modal-body">
        <div class="portlet light bordered">
            <!--- <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> </span>
                </div>
            </div> --->
            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group">
                            <label>OrderID</label>
                            <input type="hidden" name="orderID" value="" id="orderID">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Order ID" id="order-ID" disabled=""> </div>
                        </div>
                        <div class="form-group">
                            <label>Customer ID</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="Customer ID" id="customerID" disabled=""> </div>
                        </div>
                        <div class="form-group">
                            <label>Order Status</label>
                            <select class="form-control" id="order-status">
                                <cfoutput query="rc.orderStatusData">
                                    <option value="#orderStatusID#">#orderStatus#</option>
                                </cfoutput>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" id="save-status">Save changes</button>
    </div>
</div>