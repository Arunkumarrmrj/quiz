﻿component output="false" extends="base" {
	
	public any function default(rc){
		rc.roleData = Application.loginDAO.roleList();
	}

	

	public any function role_permission(rc) {
		rc.roles = Application.loginDAO.roleList();
		rc.modules = Application.module;
		rc.actions = [ "Add","Edit","View","Delete","Nav" ];
		
	}

	public any function ajax_permissonsByRole(rc) {
		param name="rc.roleID" default="";
		rc.permissons = Application.permissionDAO.getPermissionsByRole( roleID = rc.roleID );
		return variables.fw.renderData( "json", rc.permissons );
	}


	public any function save_role_permissions(rc) {
		var arrActions = [ "Add","Edit","View","Delete","Nav" ];
		var arrModules = listToArray(rc.modulename);

		if( structKeyExists(rc, "role") and isNumeric(rc.role) ) {
			Application.permissionDAO.deleteRolePermisson( roleID = rc.role ) ;
			var modules = rc.name;

			for( module in arrModules ) {
				var theStruct = {};
				theStruct["roleID"] = rc.role;
				theStruct["modulename"] = module;

				for( action in arrActions ) {
					theStruct["add"] = structKeyExists(modules, module ) and structKeyExists( modules[module], "add" ) ? modules[module]["add"] : 0;
					theStruct["edit"] = structKeyExists(modules, module ) and structKeyExists( modules[module], "edit" ) ? modules[module]["edit"] : 0;
					theStruct["view"] = structKeyExists(modules, module ) and structKeyExists( modules[module], "view" ) ? modules[module]["view"] : 0;
					theStruct["delete"] = structKeyExists(modules, module ) and structKeyExists( modules[module], "delete" ) ? modules[module]["delete"] : 0;
					theStruct["nav"] = structKeyExists(modules, module ) and structKeyExists( modules[module], "nav" ) ? modules[module]["nav"] : 0;
				}
				Application.permissionDAO.createRolePermisson( argumentcollection = theStruct );
			}
		}
		variables.fw.redirect('permission.role_permission');
	}
	
	
	public any function user_permission(rc) {
		rc.actions = [ "Add","Edit","View","Delete","Nav" ];
		rc.users = Application.employeeDAO.read();
		rc.modules = Application.module;
	}


	public any function ajax_permissonsByUser(rc) {
		param name="rc.userID" default="0";
		rc.permissions = Application.permissionDAO.getPermissionsByUserID( userID = rc.userID );

		var permissionStruct = {};
		rc.actions = ["add","edit","view","delete","nav"];
		for( permission in rc.permissions ) {
			var temp = {};
			for( action in rc.actions ) {
				structInsert( temp, action, permission[action] );
			}
			structInsert( permissionStruct, permission.modulename, temp );
			var extras = Application.permissionDAO.getExtraPermissionByUserID( userID = rc.userID, modulename = permission.modulename );
			for( extra in extras ) {
				permissionStruct[permission.modulename][extra.action_name] = extra.value;				
			}
		}		

		return variables.fw.renderData("json", permissionStruct );
	}


	public any function save_user_permissions(rc) {
		param name="rc.user" default="0";

		if( structKeyExists(rc, "user") and isNumeric(rc.user) ) {
		 	var arrActions = [ "Add","Edit","View","Delete","Nav" ] ;
			Application.permissionDAO.deleteExtraPermission( userID = rc.user ) ;
			var modules = rc.name;

			for( module in modules ) {
				for( action in arrActions ) {
					var theStruct = {};
					theStruct["userID"] = rc.user;
					theStruct["modulename"] = module;
					theStruct["action_name"] = action;
					theStruct["value"] = structKeyExists(modules, module ) and structKeyExists( modules[module], action ) ? modules[module][action] : 0;
					Application.permissionDAO.createExtraPermission( argumentcollection = theStruct );
				}
			}
			
		}
		variables.fw.redirect('permission.user_permission');
	}
	
	
}