﻿component output="false" extends="base" {
	
	public any function default(rc){
		rc.modulename = "question";
		rc.questionData = Application.questionDAO.questionlist();
		// rc.questionStatusData = Application.questionDAO.readquestionStatus();
	}

	

	public any function addedit(rc) {
		rc.roleData = Application.loginDAO.roleList();
		rc.malePrice = Application.orderDAO.priceList(catID = 1);
		rc.femalePrice = Application.orderDAO.priceList(catID = 2);
		rc.otherPrice = Application.orderDAO.priceList(catID = 3);	
	}

	public any function savequestion(rc) {
		// writeDump(rc);abort;
		if(structkeyexists(rc,'userID') and val(rc.userID)){
			// writeDump(rc);abort;
			orderID = Application.orderDAO.orderCreate(userID = rc.userID, orderDate = now(), orderStatus = 1, createdBy = session.userID, deliveryID = rc.deliveryID);
			priceIDArray = listtoarray(rc.price);
			for(price in priceIDArray){
				Application.orderDAO.createOrderdetail(orderID = orderID, productID = price, productCount = rc['price_#price#']);
			}
		}
		//rc.msg = "info:::You don't have permission";
		variables.fw.redirect("order");
	}
	
	
	
	
	
}