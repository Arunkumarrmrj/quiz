component output="false" extends="base" {
	
	public any function default(rc){
		rc.modulename = 'Category';
		rc.catData = Application.categoryDAO.categoryList();
	}

	public any function addedit(rc){
		param name="url.id" default="0";
		rc.qData = Application.categoryDAO.categoryList( id = id );
	}

	public any function save(rc) {
		if(structkeyexists(rc,'catID') AND len(rc.catID)){
			Application.categoryDAO.updateCategoryInfo(catID = rc.catID, catName = rc.catName);
			variables.fw.redirect('category');
		}
		else {
			Application.categoryDAO.insertCategoryInfo(catName = rc.catName);
			variables.fw.redirect('category');
		}
		
	}
	
}