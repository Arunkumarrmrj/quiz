component output="false" extends="base" {
	
	public any function default(rc){
		rc.modulename = "Price";
		rc.priceData = Application.priceDAO.priceList();
	}

	public any function addedit(rc){
		param name="url.id" default="0";
		rc.catList = Application.categoryDAO.categoryList();
		rc.priceData = Application.priceDAO.priceList( id = id );
	}

	public any function save(rc) {
		if(structkeyexists(rc,'priceID') AND len(rc.priceID)){
			Application.priceDAO.updatePriceInfo(priceID = rc.priceID, catID = rc.catID, priceName = rc.priceName, price = rc.price);
			variables.fw.redirect('price');
		}
		else {
			Application.priceDAO.insertPriceInfo(catID = rc.catID, priceName = rc.priceName, price = rc.price);
			variables.fw.redirect('price');
		}
	}
	
}