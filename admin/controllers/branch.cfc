﻿component output="false" extends="base" {
	
	public any function default(rc){
		rc.branchData = Application.loginDAO.branchList();
	}

	

	public any function addedit(rc) {
		param name="id" default="0";
		rc.branchData = Application.loginDAO.branchList(id = id);
		// writeDump(rc);abort;
	}

	
	public any function branchSave(rc) {
		if(structkeyexists(rc, 'id') AND val(rc.id)){
			Application.loginDAO.branchUpdate(id = rc.id, branchName = rc.branchName);
			rc.msg = "success::Branch updated successfully";
		}else if(structkeyexists(rc,'branchName') AND len(rc.branchName)){
			Application.loginDAO.branchSave(branchName = rc.branchName);
			rc.msg = "success::Branch save successfully";
		}
		variables.fw.redirect('branch', "msg");
	}
	
	
	
}