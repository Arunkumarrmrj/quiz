﻿component output="false" extends="base" {
	
	public any function default(rc){
		
	}

	public any function login_process(rc){
		param name="rc.mobilenumber" default="";
		param name="rc.password" default="";

		rc.getEmpInfo = Application.loginDAO.login(mobileNumber = rc.mobilenumber, password = rc.password);
		Application.permissionDAO.setModulePermissions( userID = rc.getEmpInfo.empID, roleID = rc.getEmpInfo.roleID );

		if(rc.getEmpInfo.recordcount EQ 1){
			session.userID = rc.getEmpInfo.empID;
			session.userName = rc.getEmpInfo.empName;
			if( rc.getEmpInfo.rolename is 'admin' ) 
				session.isAdmin = 1;
			else 
				session.isAdmin = 0;

			variables.fw.redirect("main.default");
		}else {
			rc.msg = "danger:::You don't have permission";
			variables.fw.redirect("login", "msg");
		}
	}

	public any function forgotPassword_process(rc){

		rc.qData = Application.usersDAO.read(username = rc.email);

		if(rc.qData.recordcount){

			rc.emailsend= {};
			rc.emailsend.emailTemplate = "forgotPassword";
			rc.emailsend.fromAddress = "noreply@kyrotm.com";
			rc.emailsend.replacementWords = {};
			rc.emailsend.replacementWords.password = rc.qData.pwd;
			rc.emailsend.replacementWords.username = rc.qData.username;
			rc.emailsend.toAddresses = rc.qData.username;
			rc.emailsend.subject = "Fly-Ins Forgot Password";
			
			
			Application.emailservice.sendEmail(argumentcollection = rc.emailsend);
			rc.msg = "success:::Your password successfully send on your email. Please check your email";
		} else {
			rc.msg = "danger:::Incorrect Email Address";
		}
		variables.fw.redirect("main.forgotPassword", "msg");
	}

	public any function logout(rc){
		structClear(session);
		variables.fw.redirect('login');
	}	
	
	
}