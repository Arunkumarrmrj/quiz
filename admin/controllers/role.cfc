﻿component output="false" extends="base" {
	
	public any function default(rc){
		rc.roleData = Application.loginDAO.roleList();
	}

	

	public any function addedit(rc) {
		param name="id" default="0";
		rc.roleData = Application.loginDAO.roleList(id = id);
		// writeDump(rc);abort;
	}

	
	public any function roleSave(rc) {
		if(structkeyexists(rc, 'id') AND val(rc.id)){
			Application.loginDAO.roleUpdate(id = rc.id, roleName = rc.roleName);
			rc.msg = "success::Role updated successfully";
		}else if(structkeyexists(rc,'roleName') AND len(rc.roleName)){
			Application.loginDAO.roleSave(roleName = rc.roleName);
			rc.msg = "success::Role save successfully";
		}
		variables.fw.redirect('role', "msg");
	}
	
	
	
}