﻿component output="false" extends="base" {
	
	public any function default(rc){
		rc.modulename = "Employee";
		rc.empData = Application.EmployeeDAO.read();
	}

	

	public any function addedit(rc) {
		rc.roleData = Application.loginDAO.roleList();
		rc.branchData = Application.loginDAO.branchList();
		
	}

	public any function save(rc) {
		// writeDump(rc);abort;
		Application.EmployeeDAO.empSave(empName = rc.empName, empEmail = rc.empEmail, empMobile = rc.empMobile, empPassword = rc.empPassword, empGender = rc.empGender, empDOB = rc.empDOB, empStreet = rc.empAddress, empCity = rc.empCity, empZip = rc.empZip, empRole = rc.empRole, empBranchID = rc.empBranchID, empQualification = rc.empQualification, empEmergencyContactName = rc.empEmergencyContactName, empEmergencyContactMobile = rc.empEmergencyContactMobile, empBloodGroup = rc.empBloodGroup);
		variables.fw.redirect("employee");
	}
	
	
	
	
	
}