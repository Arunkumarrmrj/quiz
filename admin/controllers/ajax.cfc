﻿component output="false" extends="base" {
	
	public any function ordersearch(rc){
		userData = Application.customersDAO.orderSearch(mobilenumber = rc.mobile);
		//data = {};
		temparray = [];
		if(userData.recordcount){
			for(i=1;i lte userData.recordcount;i = (i+1)){
				temp = {};
				temp['userID'] = userData['userID'][i];
				temp['name'] = userData['name'][i];
				temp['mobile'] = userData['mobile'][i];
				temp['cityname'] = userData['cityname'][i];
				temp['vcid'] = userData['vcID'][i];
				arrayappend(temparray, temp);
			}
		}
		//data['results'] = temparray;
		variables.fw.renderData( "json", temparray );
	}

	public any function vieworderdetail(rc){
		orderData = Application.orderDAO.vieworder(orderid = rc.orderid);
		temparray = [];
		if(orderData.recordcount){
			for(i=1;i lte orderData.recordcount;i = (i+1)){
				temp = {};
				temp['productName'] = orderData['pricename'][i];
				temp['productCount'] = orderData['productCount'][i];
				temp['price'] = orderData['price'][i];
				arrayappend(temparray, temp);
			}
		}
		variables.fw.renderData( "json", temparray );
	}

	public any function updatestatus(rc){
		// writeDump(rc);abort;
		temparray = [];
		temp['status'] = false;
		if(structkeyexists(rc,'orderid') AND structkeyexists(rc,'statusid')){
			Application.orderDAO.updateOrderStatus(orderid = rc.orderid, statusid = rc.statusid);
			temp['status'] = true;
		}
		arrayappend(temparray, temp);
		variables.fw.renderData( "json", temparray );
	}
	
}