﻿component output="false" extends="base" accessors=true {

	public any function default(rc){
		rc.modulename = "customers";
		rc.customerData = Application.customersDAO.readUser();
	}

	public any function addedit(rc){
		rc.cityData = Application.customersDAO.cityData();
		rc.customerPassword = Application.utilService.genPassword();
	}

	public any function customersave(rc){
		customerID = Application.customersDAO.createUser(name = rc.customername, email = rc.customeremail, mobilenumber = rc.customermobile, password = rc.customerpassword, cityID = rc.customercityID);
		cCode = Application.customersDAO.cityData(cityID = rc.customercityID);
		if(val(customerID)){
			newVCID = "#cCode.cityCode##customerID#";
			Application.customersDAO.saveVCID(vcID = newVCID, userID = customerID);
			smsStatus = Application.smsService.sendSMS(authkey = Application.msgAuthkey, mobilenumber = rc.customermobile, msg="Thank you for signup on Vclen. UserName: #rc.customermobile# and Password: #rc.customerpassword# http://vcleanlaundry.com");
			smsStatus = true;
			if(smsStatus){
				variables.fw.redirect('customers');
			}
		}
	}

	
	
	
	
	
	
}