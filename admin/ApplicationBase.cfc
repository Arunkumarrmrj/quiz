﻿component extends="framework.one" output="false"{

	this.name = "vclean-admin";
	this.sessionManagement = true;
	this.sessionTimeout = CreateTimeSpan(1, 23, 59, 59);
	// this.mappings["/model"] = expandpath('..')&'/model'; 


	variables.framework = {
		defaultSection = 'login',
		defaultItem = 'default',
		home = 'login.default',
		reloadApplicationOnEveryRequest = true
	};

	
	variables.framework.environments = {
	  dev = { reloadApplicationOnEveryRequest = true },
	  prod = { password = "supersecret" }
	};



	function setupRequest(){
		
		Application.name = "Quize";
		// Application.root = "/Alfaleh/admin";
		Application.DSN = "quizDSN";
		Application.dateFormat  = "dd/mm/yyyy";
		Application.msgAuthkey  = "165642AQJftkqBhY596c7f4e";
		Application['iconStr'] = {};

		param name="request.inlineJS" default="";
		param name="session.userId" default="0"; 
	 	param name="session.userName" default="";

		// if(request.section does not contain 'login' and session.userid eq '0') {
 	 		//location(url="index.cfm?action=login",addtoken='false');
		// }

		var cfcs = ["login", "Customers", "category", "permission", "employee", "question" ];

		for( cfc in cfcs ) {
			Application[cfc&'DAO'] = createobject("component","model.DAO.#cfc#DAO").init(Application.DSN);			
		}

		icons = ["icon-star", "icon-pin",  "icon-users", "icon-moustache", "icon-magic-wand", "icon-basket"];
		Application.navMenu = [ "Branch", "Role", "Employee", "Customers"];
		Application.module = [ "Branch", "Role", "Employee", "Customers"];
		Application.utilService = createobject("component","model.services.util");
		Application.smsService = createobject("component","model.services.sms");
		j = 1;

		for(m in Application.navMenu){
			structinsert(Application.iconStr, '#m#', '#icons[j]#');
			j = j+1;
		}
	}

}