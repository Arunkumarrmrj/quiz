﻿<cfprocessingdirective pageEncoding="utf-8" />
<cfoutput>	
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <cfoutput>
            <title>#ucase(getsection())#</title>
        </cfoutput>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="./assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="http://easyautocomplete.com/dist/easy-autocomplete.min.css" rel="stylesheet" />
        <link href="http://easyautocomplete.com/dist/easy-autocomplete.themes.min.css" rel="stylesheet" />
        <!-- END PAGE LEVEL PLUGINS -->
        
        <link href="./assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="./assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="./assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="./assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="./assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="./assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="./assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->		

    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
		<div class="page-wrapper">
			<!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="index.html">
                            <img src="./assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> 7 </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="##637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span> New user registered. </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="./assets/layouts/layout/img/avatar3_small.jpg" />
                                    <span class="username username-hide-on-mobile"> <cfoutput>#session.userName#</cfoutput> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<cfoutput>#buildurl('employee.addedit?id=#session.userID#')#</cfoutput>">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="<cfoutput>#buildURL('login.logout')#</cfoutput>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!--- <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li> --->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
            	<!-- <cfinclude template="sidebar.cfm"> -->
            	<!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                    	<cfinclude template="themesetting.cfm">
                    	<!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="<cfoutput>#buildurl('main')#</cfoutput>">HOME</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <cfoutput><span>#ucase(getsection())#</span></cfoutput>
                                </li>
                            </ul>
                            <cfif getsection() eq 'main'>
                                <div class="page-toolbar">
                                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                        <i class="icon-calendar"></i>&nbsp;
                                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                </div>
                            </cfif>    
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- END PAGE HEADER-->
                        <cfif structkeyexists(rc,'msg') AND len(rc.msg)>
                            <div class="custom-alerts alert alert-<cfoutput>#listgetat(rc.msg, 1, '::')#</cfoutput> fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <div><cfoutput>#listgetat(rc.msg, 2, '::')#</cfoutput></div>
                            </div>
                        </cfif>
                		<cfoutput>#body#</cfoutput>
                    </div>
                </div>
                <cfinclude template="quicksidebar.cfm"> 	
            </div>
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> <cfoutput>#year(now())#</cfoutput> &copy; By
                    <a target="_blank" href="http://netzhub.com">NetzHub</a> &nbsp;
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
		</div>	
		
		<!-- BEGIN CORE PLUGINS -->
        <script src="./assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="http://easyautocomplete.com/dist/jquery.easy-autocomplete.min.js"></script>
        <script src="./assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="./assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="./assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="./assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="./assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="./assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="./assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="./assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="./assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="./assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="./assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        <cfscript>
            request.jsDataFromCF['fw1SectionURL'] =  buildurl("#getSection()#");
            request.jsDataFromCF['fw1DeleteURL'] =  buildurl("#getSection()#.delete");
            request.jsDataFromCF['currentFW1action'] =  rc.action;
        </cfscript>  
        <script type="text/javascript">
            request = {};
            request.jsDataFromCF = #serializeJSON(request.jsDataFromCF)#;
        </script>
        <!---<script src="./assets/js/app.js"></script>  --->
        #request.inlineJS#
        
        <script type="text/javascript">
            $(function(){
                var options = {
                    placeholder: "Please enter the customer mobile number or customer ID",
                    url: function(phrase) {
                        return "index.cfm?action=ajax.ordersearch";
                    },

                    getValue: function(element) {
                        console.log(element);
                        var v = ""+element.name+", "+element.mobile
                        return v;
                    },

                    ajaxSettings: {
                        dataType: "json",
                        method: "POST",
                        data: {}
                    },

                    preparePostData: function(data) {
                        console.log(data);
                        data['mobile'] = $(".typeahead").val();
                        console.log(data);
                        return data;
                    },

                    requestDelay: 700,
                    list: {

                        onChooseEvent: function() {
                            var value = $(".typeahead").getSelectedItemData();
                            //if(value.length){
                                //alert(1)
                                $('##userInfo').removeClass('hide');
                                $('##cName').val(value.name);
                                $('##cID').val(value.vcid);
                                $('##cMobile').val(value.mobile);
                                $('##cCity').val(value.cityname);
                                $('##userID').val(value.userID);
                            //}
                            console.log(value)
                        }
                    }
                };
                $('.typeahead').easyAutocomplete(options);
                $('.priceChkBox').change(function(){
                  var t = $(this).prop('checked');
                  var v = $(this).val();
                  if(t){
                    $("input[name='price_"+v+"']").prop('disabled', false);
                    $("input[name='price_"+v+"']").focus();
                    $("input[name='price_"+v+"']").change(function(){
                      var q = $(this).val();
                      var p = $(this).data('price');
                      $('##priceAmount'+v).val(parseInt(q)*parseInt(p));
                    })
                  }else{
                    $("input[name='price_"+v+"']").prop('disabled', true);
                    $("input[name='price_"+v+"']").val('');
                    $('##priceAmount'+v).val('');
                  }
                })

                $('.view-order').click(function(){
                  var orderID = $(this).data('orderid');
                  $.post("index.cfm?action=ajax.vieworderdetail",{orderid:orderID},function(data){
                    var temp = 0;
                    var d = "";
                    for(var i=0;i<data.length;i++){
                      var c = parseInt(data[i]['productCount']);
                      var p = parseInt(data[i]['price']);
                      var total = c*p;
                      temp +=total;
                      console.log(total);
                      var l = "<tr><td>"+data[i]['productName']+"</td><td>"+data[i]['price']+"</td><td>"+data[i]['productCount']+"</td><td>"+total+"</td></tr>"
                      d +=l;
                    }
                    d +="<tr><td></td><td></td><td>Total Amount</td><td>"+temp+"</td></tr>"
                    $('##pData').html('');
                    $('##pData').append(d);
                    $('##responsive').modal('show');
                  })
                })

                $('.update-status').click(function(){
                  var orderID = $(this).data('orderid');
                  var statusID = $(this).data('status');
                  console.log(statusID);
                  var vcid = $(this).data('vcid');
                  $('##status-update').modal('show');
                  $('##orderID').val('').val(orderID);
                  $('##order-ID').val('').val(orderID);
                  $('##customerID').val('').val(vcid);
                  $('##order-status').val(statusID);
                })

                $('##save-status').click(function(){
                  var o = $('##orderID').val();
                  var s = $('##order-status').val();
                  $('##status-update').modal('hide');
                  $.post('index.cfm?action=ajax.updatestatus',{orderID:o,statusid:s},function(data){})
                })

                $('.cancel-btn').click(function(){
                    window.history.back();
                })
            })
        </script>
    </body>
</html>
</cfoutput>