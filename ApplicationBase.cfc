﻿component extends="framework.one" output="false"{

	this.name = "vclean-client";
	this.sessionManagement = true;
	this.sessionTimeout = CreateTimeSpan(1, 23, 59, 59);
	// this.mappings["/model"] = expandpath('..')&'/model'; 


	variables.framework = {
		defaultSection = 'main',
		defaultItem = 'default',
		home = 'main.default',
		generateSES = True,
		SESOmitIndex = False,
		reloadApplicationOnEveryRequest = true
	};

	
	variables.framework.environments = {
	  dev = { reloadApplicationOnEveryRequest = true },
	  prod = { password = "supersecret" }
	};



	function setupRequest(){
		
		Application.name = "vclean-client";
		// Application.root = "/Alfaleh/admin";
		Application.DSN = "vcleanDSN";
		Application.dateFormat  = "dd/mm/yyyy";
		Application.msgAuthkey  = "165642AQJftkqBhY596c7f4e";

		var cfcs = [ "user", "contactus", "price" ];

		for( cfc in cfcs ) {
			Application[cfc&'DAO'] = createobject("component","model.DAO.#cfc#DAO").init(Application.DSN);			
		}

		Application.smsService = createobject("component","model.services.sms");
		// // Application.i18nDAO = createobject("component","model.DAO.i18nDAO").init(Application.DSN);
		
		// Application.i18n = createObject("component","model.i18n").init("en");
		
		
		// variables.i18n = Application.i18n;

		// Application.shopkeeperDAO = createobject("component","model.DAO.shopkeeperDAO").init(Application.DSN);
		// Application.customerDAO = createobject("component","model.DAO.customerDAO").init(Application.DSN);
		// Application.moneyDAO = createobject("component","model.DAO.moneyDAO").init(Application.DSN);
	}

	function onMissingView(rc) {
	  if(structKeyExists(rc, "ajaxdata") && isAjaxRequest()) {
	    request.layout = false;
	    content type="application/json";
	    return serializeJSON(rc.ajaxdata);
	  }else{
	  	variables.fw.redirect("main.error");
	  }
	}

	function isAjaxRequest() {
	  var headers = getHttpRequestData().headers;
	  return structKeyExists(headers, "X-Requested-With") 
	         && (headers["X-Requested-With"] eq "XMLHttpRequest");
	}

}