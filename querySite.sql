/*

SQLyog Ultimate v8.55 
MySQL - 5.5.16 : Database - quizdb

*********************************************************************

*/



/*!40101 SET NAMES utf8 */;



/*!40101 SET SQL_MODE=''*/;



/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`quizdb` /*!40100 DEFAULT CHARACTER SET latin1 */;



USE `quizdb`;



/*Table structure for table `answer` */



DROP TABLE IF EXISTS `answer`;



CREATE TABLE `answer` (
  `answersID` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `Addeed_by` int(11) DEFAULT NULL,
  `date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Edited_by` int(11) DEFAULT NULL,
  `Date_Edited` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*Data for the table `answer` */



/*Table structure for table `group` */



DROP TABLE IF EXISTS `group`;



CREATE TABLE `group` (
  `question_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `isdelete` tinyint(1) DEFAULT NULL,
  `Added_By` int(11) DEFAULT NULL,
  `Date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Edited_By` int(11) DEFAULT NULL,
  `Date_Edited` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`question_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*Data for the table `group` */



/*Table structure for table `question_group` */



DROP TABLE IF EXISTS `question_group`;



CREATE TABLE `question_group` (
  `id` int(10) unsigned NOT NULL,
  `Question_Id` int(11) DEFAULT NULL,
  `Group_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*Data for the table `question_group` */



/*Table structure for table `questions` */



DROP TABLE IF EXISTS `questions`;



CREATE TABLE `questions` (
  `questionID` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) DEFAULT NULL,
  `questionGroup_ID` int(11) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `Added_by` int(11) DEFAULT NULL,
  `date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Edited_by` int(11) DEFAULT NULL,
  `date_Edited` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`questionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*Data for the table `questions` */



/*Table structure for table `response` */



DROP TABLE IF EXISTS `response`;



CREATE TABLE `response` (
  `ResponseID` int(11) NOT NULL AUTO_INCREMENT,
  `ResponseAnswers_ID` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ResponseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*Data for the table `response` */



/*Table structure for table `users` */



DROP TABLE IF EXISTS `users`;



CREATE TABLE `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `First_Name` varchar(255) DEFAULT NULL,
  `Last_Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



/*Data for the table `users` */



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;

/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

